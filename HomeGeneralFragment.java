package net.overc.mobile.home;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import net.overc.mobile.R;
import net.overc.mobile.base.BaseFragment;
import net.overc.mobile.core.tree.common.IRecyclerItemTouchListener;
import net.overc.mobile.database.models.home.HomeActiveMonitorsBean;
import net.overc.mobile.database.models.home.HomeAlertsCountBean;
import net.overc.mobile.database.models.home.HomeCompareToBean;
import net.overc.mobile.database.models.home.HomeEventsBean;
import net.overc.mobile.database.models.home.HomeLoneWorkerAlertsCountBean;
import net.overc.mobile.database.models.home.HomeOverviewGraphBean;
import net.overc.mobile.database.models.home.HomeOverviewKPIGraphBean;
import net.overc.mobile.database.models.home.HomeStatusActiveBean;
import net.overc.mobile.database.models.home.HomeTopFiveBean;
import net.overc.mobile.home.adapters.CustomSpinnerAdapter;
import net.overc.mobile.home.adapters.HeaderWhiteFilterAdapter;
import net.overc.mobile.home.adapters.TopFiveAdapter;
import net.overc.mobile.home.chart.ChartEntry;
import net.overc.mobile.home.chart.ChartHelper;
import net.overc.mobile.home.chart.ChartMarkerData;
import net.overc.mobile.home.chart.ChartMarkerView;
import net.overc.mobile.home.chart.ChartRanges;
import net.overc.mobile.home.chart.DayValueFormatter;
import net.overc.mobile.home.chart.MonthValueFormatter;
import net.overc.mobile.home.chart.YearValueFormatter;
import net.overc.mobile.http.RestService;
import net.overc.mobile.markerinterface.IBaseEntity;
import net.overc.mobile.ui.view.ComparedView;
import net.overc.mobile.ui.view.HomeCountView;
import net.overc.mobile.ui.view.MagicProgressCircle;
import net.overc.mobile.ui.view.ProgressView;
import net.overc.mobile.ui.view.ViewNoData;
import net.overc.mobile.utils.DateUtils;
import net.overc.mobile.utils.ResUtils;
import net.overc.mobile.utils.RxUtils;
import net.overc.mobile.utils.StringUtils;
import net.overc.mobile.utils.ViewUtils;
import net.overc.mobile.utils.injector.Layout;
import net.overc.mobile.utils.managers.ResManager;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import rx.Subscription;

@Layout(R.layout.frg_home_general)
public class HomeGeneralFragment extends BaseFragment
        implements IRecyclerItemTouchListener<IBaseEntity>,
        TabLayout.OnTabSelectedListener {

    public static final int DAY = 0;
    public static final int MONTH = 1;
    public static final int YEAR = 2;

    private static final int TOP_FIVE_ACCOUNTS = 0;
    private static final int TOP_FIVE_USERS = 1;
    private static final int TOP_FIVE_SENSORS = 2;


    private static final int GRAPH_OVERVIEW = 0;
    private static final int GRAPH_KPI = 1;

    private static final int LONE_WORKER_ALERTS = 0;
    private static final int LONE_WORKER_COMPARE = 1;

    private static final int MONITORS_ALERTS = 0;
    private static final int ACTIVE_MONITORS = 1;


    @BindView(R.id.iv_header_home_calendar)
    ImageView ivCalendar;
    @BindView(R.id.tv_header_home_date)
    TextView tvDate;
    @BindView(R.id.spn_header_home_filter)
    Spinner spnFilter;
    @BindView(R.id.sv_home_general)
    ScrollView svContainer;
    @BindView(R.id.pv_home_general_events)
    ProgressView pvEvents;
    @BindView(R.id.tv_home_general_events_multimedia)
    TextView tvEventsMultimedia;
    @BindView(R.id.tv_home_general_events_login_logoff)
    TextView tvEventsLoginLogoff;
    @BindView(R.id.tv_home_general_events_notification_emails)
    TextView tvEventsNotificationEmails;
    @BindView(R.id.tv_home_general_events_reports_generated)
    TextView tvEventsReportsGenerated;
    @BindView(R.id.tv_home_general_events_audit_reports)
    TextView tvEventsAuditReports;
    @BindView(R.id.tv_home_general_events_sms)
    TextView tvEventsSms;
    @BindView(R.id.pv_home_general_status_active)
    ProgressView pvStatusActive;
    @BindView(R.id.mpc_home_general_status_active_progress)
    MagicProgressCircle mpcStatusActive;
    @BindView(R.id.tv_home_general_status_active_progress)
    TextView tvStatusActive;
    @BindView(R.id.hcv_home_general_status_active_sensors)
    HomeCountView hcvStatusActiveSensors;
    @BindView(R.id.hcv_home_general_status_active_locations)
    HomeCountView hcvStatusActiveLocations;
    @BindView(R.id.hcv_home_general_status_active_users)
    HomeCountView hcvStatusActiveUsers;
    @BindView(R.id.hcv_home_general_status_active_devices)
    HomeCountView hcvStatusActiveDevices;
    @BindView(R.id.hcv_home_general_status_active_accounts)
    HomeCountView hcvStatusActiveAccounts;
    @BindView(R.id.tl_home_general)
    TabLayout tlChartType;
    @BindView(R.id.spn_home_general_chart)
    Spinner spnChartFilter;
    @BindView(R.id.lc_overview_kpi_chart)
    LineChart lcOverviewKpiChart;
    @BindView(R.id.pv_overview_kpi_chart)
    ProgressView pvOverviewKpi;
    @BindView(R.id.spn_home_general_top_five)
    Spinner spnTopFiveFilter;
    @BindView(R.id.rv_home_general_top_five)
    RecyclerView rvTopFiveList;
    @BindView(R.id.pv_home_general_top_five)
    ProgressView pvTopFive;
    @BindView(R.id.vnd_home_general_top_five)
    ViewNoData vnEmptyTopFive;
    @BindView(R.id.tv_home_general_compared_to)
    TextView tvComparedTo;
    @BindView(R.id.cv_home_general_alerts)
    ComparedView cvAlerts;
    @BindView(R.id.tv_home_general_compared_alerts)
    TextView tvComparedAlerts;
    @BindView(R.id.cv_home_general_sensor_events)
    ComparedView cvSensorEvents;
    @BindView(R.id.tv_home_general_compared_sensor_events)
    TextView tvComparedSensorEvents;
    @BindView(R.id.cv_home_general_emails)
    ComparedView cvEmails;
    @BindView(R.id.tv_home_general_compared_emails)
    TextView tvComparedEmails;
    @BindView(R.id.cv_home_general_reports)
    ComparedView cvReports;
    @BindView(R.id.tv_home_general_compared_reports)
    TextView tvComparedReports;
    @BindView(R.id.cv_home_general_new_accounts)
    ComparedView cvNewAccounts;
    @BindView(R.id.tv_home_general_compared_new_accounts)
    TextView tvComparedNewAccounts;
    @BindView(R.id.cv_home_general_sms)
    ComparedView cvSMS;
    @BindView(R.id.tv_home_general_compared_sms)
    TextView tvComparedSMS;
    @BindView(R.id.pv_home_general_compare_to)
    ProgressView pvCompareTo;
    @BindView(R.id.spn_home_general_monitors)
    Spinner spnMonitor;
    @BindView(R.id.pv_home_general_monitors_monitors_alerts)
    ProgressView pvActiveMonitorsMonitorsAlerts;
    @BindView(R.id.ll_home_general_monitors_alerts)
    LinearLayout llMonitorsAlerts;
    @BindView(R.id.hcv_home_general_monitors_alerts_geofence)
    HomeCountView hcvMonitorsAlertsGeofence;
    @BindView(R.id.hcv_home_general_monitors_alerts_lone_worker)
    HomeCountView hcvMonitorsAlertsLoneWorker;
    @BindView(R.id.hcv_home_general_monitors_alerts_sensor_task)
    HomeCountView hcvMonitorsAlertsSensorTask;
    @BindView(R.id.pc_home_general_monitors_alerts_chart)
    PieChart pcMonitorsAlertsChart;
    @BindView(R.id.ll_home_general_active_monitors)
    LinearLayout llActiveMonitors;
    @BindView(R.id.mpc_home_general_active_monitors_progress)
    MagicProgressCircle mpcActiveMonitors;
    @BindView(R.id.tv_home_general_active_monitors_progress)
    TextView tvActiveMonitorsProgress;
    @BindView(R.id.hcv_home_general_monitor_active_lone_worker)
    HomeCountView hcvActiveMonitorsLoneWorker;
    @BindView(R.id.hcv_home_general_monitor_active_geofence)
    HomeCountView hcvActiveMonitorsGeofence;
    @BindView(R.id.hcv_home_general_monitor_active_sensor_task)
    HomeCountView hcvActiveMonitorsSensorTask;
    @BindView(R.id.hcv_home_general_monitor_active_multimedia)
    HomeCountView hcvActiveMonitorsMultimedia;
    @BindView(R.id.hcv_home_general_monitor_active_audit_report)
    HomeCountView hcvActiveMonitorsAudit;
    @BindView(R.id.spn_home_general_lone_worker_alerts)
    Spinner spnLoneWorkerAlerts;
    @BindView(R.id.ll_home_general_lone_worker_alerts)
    LinearLayout llLoneWorkerAlerts;
    @BindView(R.id.pv_home_general_lone_worker)
    ProgressView pvLoneWorker;
    @BindView(R.id.hcv_home_general_lone_worker_alerts_mandown)
    HomeCountView hcvLoneWorkerAlertsMandown;
    @BindView(R.id.hcv_home_general_lone_worker_alerts_panic_button)
    HomeCountView hcvLoneWorkerAlertsPanic;
    @BindView(R.id.hcv_home_general_lone_worker_alerts_sensor_declare)
    HomeCountView hcvLoneWorkerAlertsSensors;
    @BindView(R.id.pc_home_general_lone_worker_alerts_chart)
    PieChart pcLoneWorkerAlerts;
    @BindView(R.id.ll_home_general_lone_worker_alerts_compare)
    LinearLayout llLoneWorkerCompare;
    @BindView(R.id.cv_home_general_lone_worker_compared_panic)
    ComparedView cvLoneWorkerPanic;
    @BindView(R.id.tv_home_general_lone_worker_compared_panic)
    TextView tvLoneWorkerPanic;
    @BindView(R.id.cv_home_general_lone_worker_compared_mandown)
    ComparedView cvLoneWorkerMandown;
    @BindView(R.id.tv_home_general_lone_worker_compared_mandown)
    TextView tvLoneWorkerMandown;
    @BindView(R.id.cv_home_general_lone_worker_compared_sensor_events)
    ComparedView cvLoneWorkerSensors;
    @BindView(R.id.tv_home_general_lone_worker_compared_sensor_events)
    TextView tvLoneWorkerSensors;

    private int filterType = DAY;
    private TopFiveAdapter topFiveAdapter;
    private CustomSpinnerAdapter chartFilterAdapter;
    private Calendar toDate = Calendar.getInstance();
    private CustomSpinnerAdapter topFiveSpinnerAdapter;
    private CustomSpinnerAdapter monitorsSpinnerAdapter;
    private HeaderWhiteFilterAdapter headerFilterAdapter;
    private CustomSpinnerAdapter loneWorkerSpinnerAdapter;

    private Subscription eventsSubscription;
    private Subscription topFiveSubscription;
    private Subscription LWAlertsSubscription;
    private Subscription compareToSubscription;
    private Subscription overViewKpiSubscription;
    private Subscription statusActiveSubscription;
    private Subscription monitorsAlertsSubscription;
    private Subscription activeMonitorsSubscription;
    private Subscription LWAlertsCompareToSubscription;

    private int monitorsSpnPosition = MONITORS_ALERTS;
    private int loneWorkerSpnPosition = LONE_WORKER_ALERTS;
    private int topFiveSpnPosition = TOP_FIVE_ACCOUNTS;
    private int chartSpnPosition = ChartEntry.GRAPH_OVERVIEW_ALL;
    private int graphPosition = GRAPH_OVERVIEW;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initTabLayout(savedInstanceState);
        initLineChart();
        initAdapters();
        setListeners();
        fillView();
    }

    @Override
    public void onTouch(int position, IBaseEntity data) {

    }

    @OnItemSelected(R.id.spn_header_home_filter)
    void selectPeriod(Spinner spinner, int position) {
        switch (position) {
            case 0:
                filterType = DAY;
                break;
            case 1:
                filterType = MONTH;
                break;

            case 2:
                filterType = YEAR;
                break;
        }
        fillCompareToTitle();
        loadData();
    }

    @OnItemSelected(R.id.spn_home_general_top_five)
    void selectTopFiveType(Spinner spinner, int position) {
        topFiveSpnPosition = position;
        loadTopFive();
    }

    @OnItemSelected(R.id.spn_home_general_monitors)
    void selectMonitors(Spinner spinner, int position) {
        monitorsSpnPosition = position;
        switch (position) {
            case MONITORS_ALERTS:
                loadMonitorsAlerts();
                break;

            case ACTIVE_MONITORS:
                loadActiveMonitors();
                break;
        }
    }

    @OnItemSelected(R.id.spn_home_general_lone_worker_alerts)
    void selectLoneWorker(Spinner spinner, int position) {
        // TODO: 21.04.2017 OCTAND-536 Hide loneworker view
        loneWorkerSpnPosition = position;
//        switch (position) {
//            case LONE_WORKER_ALERTS:
//                loadLoneWorkerAlerts();
//                break;
//
//            case LONE_WORKER_COMPARE:
//                loadLoneWorkerCompareTo();
//                break;
//        }
    }

    @OnItemSelected(R.id.spn_home_general_chart)
    void selectChartType(Spinner spinner, int position) {
        chartSpnPosition = position;
        loadOverview();
    }

    @OnClick({R.id.iv_header_home_calendar, R.id.tv_header_home_date})
    void showCalendar() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DateDialogTheme,
                (view, year, monthOfYear, dayOfMonth) -> {
                    toDate.set(year, monthOfYear, dayOfMonth);
                    fillHeader();
                    loadData();
                }, toDate.get(Calendar.YEAR), toDate.get(Calendar.MONTH), toDate.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        datePickerDialog.show();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        graphPosition = tab.getPosition();
        switch (tab.getPosition()) {
            case GRAPH_OVERVIEW:
                spnChartFilter.setVisibility(View.VISIBLE);
                loadOverview();
                break;

            case GRAPH_KPI:
                spnChartFilter.setVisibility(View.GONE);
                loadSensorMonitorKPI();
                break;
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    private void initAdapters() {
        headerFilterAdapter = new HeaderWhiteFilterAdapter(ResUtils.getStringList(R.array.header_filter));
        spnFilter.setAdapter(headerFilterAdapter);

        topFiveSpinnerAdapter = new CustomSpinnerAdapter(ResUtils.getStringList(R.array.top_five_values));
        spnTopFiveFilter.setAdapter(topFiveSpinnerAdapter);

        monitorsSpinnerAdapter = new CustomSpinnerAdapter(ResUtils.getStringList(R.array.home_monitors_list));
        monitorsSpinnerAdapter.setTextAllCaps(true);
        spnMonitor.setAdapter(monitorsSpinnerAdapter);

        loneWorkerSpinnerAdapter = new CustomSpinnerAdapter(ResUtils.getStringList(R.array.home_lone_worker_list));
        loneWorkerSpinnerAdapter.setTextAllCaps(true);
        spnLoneWorkerAlerts.setAdapter(loneWorkerSpinnerAdapter);

        chartFilterAdapter = new CustomSpinnerAdapter(ResManager.getInstance().getChartOverViewList());
        chartFilterAdapter.setTextAllCaps(true);
        spnChartFilter.setAdapter(chartFilterAdapter);

        topFiveAdapter = new TopFiveAdapter(getContext());
        topFiveAdapter.setRecyclerItemTouchListener(this);
        rvTopFiveList.setLayoutManager(new LinearLayoutManager(getContext()));
        rvTopFiveList.setAdapter(topFiveAdapter);
    }

    private void initLineChart() {
        lcOverviewKpiChart.getDescription().setEnabled(false);
        XAxis xAxis = lcOverviewKpiChart.getXAxis();
        YAxis yAxis = lcOverviewKpiChart.getAxisLeft();

        lcOverviewKpiChart.getAxisRight().setEnabled(false);

        lcOverviewKpiChart.setMarker(new ChartMarkerView(getActivity()));

        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisLineWidth(1f);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setTextColor(ResUtils.getColor(R.color.home_event_value));
        xAxis.setAxisLineColor(ResUtils.getColor(R.color.graph_x_axis_color));
        xAxis.setValueFormatter(new DayValueFormatter());


        yAxis.setDrawZeroLine(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setGridColor(ResUtils.getColor(R.color.graph_x_axis_color));
        yAxis.setTextColor(ResUtils.getColor(R.color.home_event_value));
        yAxis.setAxisLineColor(ResUtils.getColor(R.color.white));
        yAxis.setAxisMinimum(0);


        Legend l = lcOverviewKpiChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        lcOverviewKpiChart.setPinchZoom(true);
        setDecimalFormatter();
    }

    private void initTabLayout(Bundle args) {
        if (args == null) {
            TabLayout.Tab tab = tlChartType.getTabAt(0);
            if (tab != null) {
                tab.select();
            }
        }
    }

    private void setListeners() {
        tlChartType.addOnTabSelectedListener(this);
        lcOverviewKpiChart.setOnTouchListener((v, event) -> {
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    // Disallow ScrollView to intercept touch events.
                    svContainer.requestDisallowInterceptTouchEvent(true);
                    break;

                case MotionEvent.ACTION_UP:
                    // Allow ScrollView to intercept touch events.
                    svContainer.requestDisallowInterceptTouchEvent(false);
                    break;
            }

            return false;
        });

        lcOverviewKpiChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                ChartRanges ranges = new ChartRanges(lcOverviewKpiChart.getXChartMin(),
                        lcOverviewKpiChart.getXChartMax(),
                        lcOverviewKpiChart.getYChartMin(), lcOverviewKpiChart.getYChartMax());
                e.setData(new ChartMarkerData(e.getY(), ranges));
            }

            @Override
            public void onNothingSelected() {

            }
        });

        pvTopFive.setReloadListener(this::loadTopFive);
        pvCompareTo.setReloadListener(this::loadCompareTo);
        pvEvents.setReloadListener(this::loadEvents);
        pvStatusActive.setReloadListener(this::loadStatusActive);
        pvOverviewKpi.setReloadListener(() -> {
            if (graphPosition == GRAPH_OVERVIEW) {
                loadOverview();
            } else {
                loadSensorMonitorKPI();
            }
        });

        pvActiveMonitorsMonitorsAlerts.setReloadListener(() -> {
            if (monitorsSpnPosition == MONITORS_ALERTS) {
                loadMonitorsAlerts();
            } else {
                loadActiveMonitors();
            }
        });

        pvLoneWorker.setReloadListener(() -> {
            if (loneWorkerSpnPosition == LONE_WORKER_ALERTS) {
                loadLoneWorkerAlerts();
            } else {
                loadLoneWorkerCompareTo();
            }
        });
    }

    private void fillView() {
        fillHeader();
        fillCompareToTitle();
    }

    private void fillHeader() {
        tvDate.setText(DateUtils.patternFormatting(toDate.getTime(), DateUtils.dd_MMM));
    }

    private void fillCompareToTitle() {
        String append = null;
        List<String> values = null;
        switch (filterType) {
            case DAY:
                append = ResUtils.getString(R.string.label_home_yesterday);
                values = ResUtils.getStringList(R.array.home_lone_worker_list);
                break;
            case MONTH:
                append = ResUtils.getString(R.string.label_home_last_month);
                values = ResUtils.getStringList(R.array.home_lone_worker_list_month);
                break;
            case YEAR:
                append = ResUtils.getString(R.string.label_home_last_year);
                values = ResUtils.getStringList(R.array.home_lone_worker_list_year);
                break;
        }

        tvComparedTo.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_compared_to), append));
        if (loneWorkerSpinnerAdapter != null) {
            loneWorkerSpinnerAdapter.setValues(values);
        }
    }

    private void loadData() {
        loadEvents();
        loadStatusActive();
        loadCompareTo();
        loadTopFive();
        if (monitorsSpnPosition == MONITORS_ALERTS) {
            loadMonitorsAlerts();
        } else {
            loadActiveMonitors();
        }
        if (loneWorkerSpnPosition == LONE_WORKER_ALERTS) {
            loadLoneWorkerAlerts();
        } else {
            loadLoneWorkerCompareTo();
        }

        if (graphPosition == GRAPH_OVERVIEW) {
            loadOverview();
        } else {
            loadSensorMonitorKPI();
        }
    }


    //    EVENTS
    private void loadEvents() {
        pvEvents.onStartLoading();
        if (eventsSubscription != null) {
            eventsSubscription.unsubscribe();
        }
        addSubscription(eventsSubscription = RestService.getInstance().getHomeEventsCount(getParams())
                .compose(RxUtils.applySchedulers())
                .subscribe(homeEventsBean -> {
                    fillEvents(homeEventsBean);
                    pvEvents.onCompleteLoading();
                }, throwable -> {
                    pvEvents.onErrorLoading(throwable.getMessage());
                }));
    }

    private void fillEvents(HomeEventsBean eventsBean) {
        tvEventsMultimedia.setText(StringUtils.longFormat(eventsBean.getMultimediaEvents()));
        tvEventsLoginLogoff.setText(StringUtils.longFormat(eventsBean.getLoginAndLogoutEvents()));
        tvEventsNotificationEmails.setText(StringUtils.longFormat(eventsBean.getEmailNotifications()));
        tvEventsReportsGenerated.setText(StringUtils.longFormat(eventsBean.getGeneratedReports()));
        tvEventsAuditReports.setText(StringUtils.longFormat(eventsBean.getAuditReports()));
        tvEventsSms.setText(StringUtils.longFormat(eventsBean.getSmsNotifications()));
    }


    //STATUS ACTIVE
    private void loadStatusActive() {
        pvStatusActive.onStartLoading();
        if (statusActiveSubscription != null) {
            statusActiveSubscription.unsubscribe();
        }
        addSubscription(statusActiveSubscription = RestService.getInstance().getHomeStatusActive()
                .compose(RxUtils.applySchedulers())
                .subscribe(bean -> {
                    fillStatusActive(bean);
                    pvStatusActive.onCompleteLoading();
                }, throwable -> {
                    pvStatusActive.onErrorLoading(throwable.getMessage());

                }));
    }

    private void fillStatusActive(HomeStatusActiveBean bean) {
        mpcStatusActive.setSmoothPercent(bean.calculateActiveItems(), 500);
        tvStatusActive.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent), bean.calculatePercentActiveItems()));
        hcvStatusActiveSensors.setValueText(bean.getActiveSensors());
        hcvStatusActiveLocations.setValueText(bean.getActiveLocations());
        hcvStatusActiveUsers.setValueText(bean.getActiveUsers());
        hcvStatusActiveDevices.setValueText(bean.getActiveDevices());
        hcvStatusActiveAccounts.setValueText(bean.getActiveAccounts());

    }

    private void loadCompareTo() {
        pvCompareTo.onStartLoading();
        if (compareToSubscription != null) {
            compareToSubscription.unsubscribe();
        }
        addSubscription(compareToSubscription = RestService.getInstance().getHomeCompareTo(getCompareParams())
                .compose(RxUtils.applySchedulers())
                .subscribe(homeCompareToBean -> {
                    fillCompareTo(homeCompareToBean);
                    pvCompareTo.onCompleteLoading();
                }, throwable -> {
                    pvCompareTo.onErrorLoading(throwable.getMessage());
                }));
    }

    private void fillCompareTo(HomeCompareToBean bean) {
        cvAlerts.setValue(bean.getPreviousCountAlerts(), bean.getCountAlerts());
        tvComparedAlerts.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent), getComparePercent(bean.getPreviousCountAlerts(), bean.getCountAlerts())));

        cvSensorEvents.setValue(bean.getPreviousCountSensorEvents(), bean.getCountSensorEvents());
        tvComparedSensorEvents.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent),
                getComparePercent(bean.getPreviousCountSensorEvents(), bean.getCountSensorEvents())));

        cvEmails.setValue(bean.getPreviousCountEmails(), bean.getCountEmails());
        tvComparedEmails.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent),
                getComparePercent(bean.getPreviousCountEmails(), bean.getCountEmails())));

        cvReports.setValue(bean.getPreviousCountReports(), bean.getCountReports());
        tvComparedReports.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent),
                getComparePercent(bean.getPreviousCountReports(), bean.getCountReports())));

        cvNewAccounts.setValue(bean.getPreviousCountNewAccounts(), bean.getCountNewAccounts());
        tvComparedNewAccounts.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent),
                getComparePercent(bean.getPreviousCountNewAccounts(), bean.getCountNewAccounts())));

        cvSMS.setValue(bean.getPreviousCountSms(), bean.getCountSms());
        tvComparedSMS.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent),
                getComparePercent(bean.getPreviousCountSms(), bean.getCountSms())));
    }

    //    ACTIVE MONITORS
    private void loadActiveMonitors() {
        pvActiveMonitorsMonitorsAlerts.onStartLoading();
        if (activeMonitorsSubscription != null) {
            activeMonitorsSubscription.unsubscribe();
        }
        addSubscription(activeMonitorsSubscription = RestService.getInstance()
                .getHomeActiveMonitors().compose(RxUtils.applySchedulers())
                .subscribe(homeActiveMonitorsBean -> {
                    pvActiveMonitorsMonitorsAlerts.onCompleteLoading();
                    fillActiveMonitors(homeActiveMonitorsBean);
                }, throwable -> {
                    pvActiveMonitorsMonitorsAlerts.onErrorLoading(throwable.getMessage());
                }));
    }

    private void fillActiveMonitors(HomeActiveMonitorsBean bean) {
        llActiveMonitors.setVisibility(View.VISIBLE);
        llMonitorsAlerts.setVisibility(View.GONE);

        hcvActiveMonitorsLoneWorker.setValueText(bean.getActiveLoneWorkerMonitors());
        hcvActiveMonitorsGeofence.setValueText(bean.getActiveGeofenceMonitors());
        hcvActiveMonitorsSensorTask.setValueText(bean.getActiveSensorMonitors());
        hcvActiveMonitorsMultimedia.setValueText(bean.getActiveMultimediaMonitors());
        hcvActiveMonitorsAudit.setValueText(bean.getActiveAuditMonitors());


        mpcActiveMonitors.setSmoothPercent(bean.calculateActiveItems(), 500);
        tvActiveMonitorsProgress.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent), bean.calculatePercentActiveItems()));
    }

//    MONITORS ALERTS

    private void loadMonitorsAlerts() {
        pvActiveMonitorsMonitorsAlerts.onStartLoading();
        if (monitorsAlertsSubscription != null) {
            monitorsAlertsSubscription.unsubscribe();
        }

        addSubscription(monitorsAlertsSubscription = RestService.getInstance().getHomeAlertsCount(getParams())
                .compose(RxUtils.applySchedulers())
                .subscribe(homeAlertsCountBean -> {
                    pvActiveMonitorsMonitorsAlerts.onCompleteLoading();
                    fillMonitorsAlerts(homeAlertsCountBean);
                }, throwable -> {
                    pvActiveMonitorsMonitorsAlerts.onErrorLoading(throwable.getMessage());
                }));
    }

    private void fillMonitorsAlerts(HomeAlertsCountBean bean) {
        llMonitorsAlerts.setVisibility(View.VISIBLE);
        llActiveMonitors.setVisibility(View.GONE);

        hcvMonitorsAlertsLoneWorker.setValueText(bean.getLoneWorkerMonitorAlerts());
        hcvMonitorsAlertsGeofence.setValueText(bean.getGeofenceMonitorAlerts());
        hcvMonitorsAlertsSensorTask.setValueText(bean.getSensorMonitorAlerts());
        fillMonitorChart(bean);
    }


    private void fillMonitorChart(HomeAlertsCountBean bean) {
        pcMonitorsAlertsChart.setUsePercentValues(false);
        pcMonitorsAlertsChart.setDrawHoleEnabled(true);
        pcMonitorsAlertsChart.setDrawHoleEnabled(false);
        pcMonitorsAlertsChart.setDrawEntryLabels(false);
        pcMonitorsAlertsChart.getDescription().setEnabled(false);
        pcMonitorsAlertsChart.setDrawCenterText(false);
        pcMonitorsAlertsChart.setRotationEnabled(true);
        pcMonitorsAlertsChart.setHighlightPerTapEnabled(true);

        setMonitorChartValue(bean);
    }

    private void setMonitorChartValue(HomeAlertsCountBean monitorChartValue) {
        ArrayList<PieEntry> entries = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();

        entries.add(new PieEntry(monitorChartValue.getLoneWorkerMonitorAlerts()));
        entries.add(new PieEntry(monitorChartValue.getSensorMonitorAlerts()));
        entries.add(new PieEntry(monitorChartValue.getGeofenceMonitorAlerts()));


        colors.add(ResUtils.getColor(R.color.chart_home_lone_worker));
        colors.add(ResUtils.getColor(R.color.chart_home_sensor_task));
        colors.add(ResUtils.getColor(R.color.chart_home_geofence));


        PieDataSet dataSet = new PieDataSet(entries, "Monitors alerts");
        dataSet.setSliceSpace(3f);
        dataSet.setDrawValues(false);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(colors);

        PieData data = new PieData(dataSet);

        pcMonitorsAlertsChart.setData(data);
        pcMonitorsAlertsChart.getLegend().setEnabled(false);
        pcMonitorsAlertsChart.invalidate();
        pcMonitorsAlertsChart.animateY(1000);
    }


    //    LONE WORKER ALERTS
    private void loadLoneWorkerAlerts() {
        pvLoneWorker.onStartLoading();
        if (LWAlertsSubscription != null) {
            LWAlertsSubscription.unsubscribe();
        }
        addSubscription(LWAlertsSubscription = RestService.getInstance().getHomeLoneWorkerAlertsCount(getParams())
                .compose(RxUtils.applySchedulers())
                .subscribe(bean -> {
                    pvLoneWorker.onCompleteLoading();
                    fillLoneWorkerAlerts(bean);
                }, throwable -> {
                    pvLoneWorker.onErrorLoading(throwable.getMessage());
                }));
    }

    private void fillLoneWorkerAlerts(HomeLoneWorkerAlertsCountBean bean) {
        llLoneWorkerAlerts.setVisibility(View.VISIBLE);
        llLoneWorkerCompare.setVisibility(View.GONE);

        hcvLoneWorkerAlertsPanic.setValueText(bean.getCountPanicAlerts());
        hcvLoneWorkerAlertsMandown.setValueText(bean.getCountManDownAlerts());
        hcvLoneWorkerAlertsSensors.setValueText(bean.getCountSensorAlerts());

        fillLoneWorkerChart(bean);
    }

    private void fillLoneWorkerChart(HomeLoneWorkerAlertsCountBean bean) {
        pcLoneWorkerAlerts.setUsePercentValues(false);
        pcLoneWorkerAlerts.setDrawHoleEnabled(true);

        pcLoneWorkerAlerts.setDrawHoleEnabled(false);
        pcLoneWorkerAlerts.setDrawEntryLabels(false);

        pcLoneWorkerAlerts.getDescription().setEnabled(false);
        pcLoneWorkerAlerts.setDrawCenterText(false);
        pcLoneWorkerAlerts.setRotationEnabled(true);
        pcLoneWorkerAlerts.setHighlightPerTapEnabled(true);

        setLoneWorkerChartValue(bean);

    }

    private void setLoneWorkerChartValue(HomeLoneWorkerAlertsCountBean bean) {
        ArrayList<PieEntry> entries = new ArrayList<>();
        ArrayList<Integer> colors = new ArrayList<>();

        entries.add(new PieEntry(bean.getCountManDownAlerts()));
        entries.add(new PieEntry(bean.getCountSensorAlerts()));
        entries.add(new PieEntry(bean.getCountPanicAlerts()));

        colors.add(ResUtils.getColor(R.color.chart_home_mandown));
        colors.add(ResUtils.getColor(R.color.chart_home_sensor_declare));
        colors.add(ResUtils.getColor(R.color.chart_home_panic));


        PieDataSet dataSet = new PieDataSet(entries, "Lone worker alerts");
        dataSet.setSliceSpace(3f);
        dataSet.setDrawValues(false);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(colors);
        PieData data = new PieData(dataSet);

        pcLoneWorkerAlerts.setData(data);
        pcLoneWorkerAlerts.getLegend().setEnabled(false);
        pcLoneWorkerAlerts.invalidate();
        pcLoneWorkerAlerts.animateY(1000);
    }

    //    LONE WORKER COMPARE
    private void loadLoneWorkerCompareTo() {
        pvLoneWorker.onStartLoading();
        if (LWAlertsCompareToSubscription != null) {
            LWAlertsCompareToSubscription.unsubscribe();
        }
        addSubscription(LWAlertsCompareToSubscription =
                RestService.getInstance().getHomeLoneWorkerAlertsCompareTo(getCompareParams())
                        .compose(RxUtils.applySchedulers())
                        .subscribe(bean -> {
                            pvLoneWorker.onCompleteLoading();
                            fillLoneWorkerCompareTo(bean);
                        }, throwable -> {
                            pvLoneWorker.onErrorLoading(throwable.getMessage());
                        }));
    }

    private void fillLoneWorkerCompareTo(HomeLoneWorkerAlertsCountBean bean) {
        llLoneWorkerCompare.setVisibility(View.VISIBLE);
        llLoneWorkerAlerts.setVisibility(View.GONE);

        cvLoneWorkerPanic.setValue(bean.getPreviousCountPanicAlerts(), bean.getCountPanicAlerts());
        tvLoneWorkerPanic.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent), getComparePercent(bean.getPreviousCountPanicAlerts(), bean.getCountPanicAlerts())));

        cvLoneWorkerSensors.setValue(bean.getPreviousCountSensorAlerts(), bean.getCountSensorAlerts());
        tvLoneWorkerSensors.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent), getComparePercent(bean.getPreviousCountSensorAlerts(), bean.getCountSensorAlerts())));

        cvLoneWorkerMandown.setValue(bean.getPreviousCountManDownAlerts(), bean.getCountManDownAlerts());
        tvLoneWorkerMandown.setText(MessageFormat.format(ResUtils.getString(R.string.label_home_percent), getComparePercent(bean.getPreviousCountManDownAlerts(), bean.getCountManDownAlerts())));
    }

//    KPI

    private void loadSensorMonitorKPI() {
        pvOverviewKpi.onStartLoading();
        if (overViewKpiSubscription != null) {
            overViewKpiSubscription.unsubscribe();
        }
        addSubscription(overViewKpiSubscription = RestService.getInstance().getHomeSensorMonitorKpi(getParams())
                .compose(RxUtils.applySchedulers())
                .subscribe(graphPoints -> {
                    pvOverviewKpi.onCompleteLoading();
                    fillKPIChart(graphPoints);
                }, throwable -> {
                    pvOverviewKpi.onErrorLoading(throwable.getMessage());
                }));

    }

    private void fillKPIChart(HomeOverviewKPIGraphBean bean) {
        setPercentFormatter();
        setYValueFormatter();
        lcOverviewKpiChart.getLegend().setEnabled(true);
        lcOverviewKpiChart.clear();
        lcOverviewKpiChart.fitScreen();
        lcOverviewKpiChart.setData(ChartHelper.getSingleChartData(bean, ChartEntry.GRAPH_KPI));
        lcOverviewKpiChart.invalidate();
        lcOverviewKpiChart.animateY(1500, Easing.EasingOption.EaseInOutQuart);

    }

    //    OVERVIEW
    private void loadOverview() {
        pvOverviewKpi.onStartLoading();
        if (overViewKpiSubscription != null) {
            overViewKpiSubscription.unsubscribe();
        }
        addSubscription(overViewKpiSubscription = RestService.getInstance().getHomeOverview(getParams())
                .compose(RxUtils.applySchedulers())
                .subscribe(homeOverviewGraphBean -> {
                    pvOverviewKpi.onCompleteLoading();
                    fillOverViewChart(homeOverviewGraphBean);
                }, throwable -> {
                    pvOverviewKpi.onErrorLoading(throwable.getMessage());
                }));
    }

    private void fillOverViewChart(HomeOverviewGraphBean bean) {
        setDecimalFormatter();
        fillLineChart(bean);
    }

    private void fillLineChart(HomeOverviewGraphBean bean) {
        lcOverviewKpiChart.getLegend().setEnabled(true);
        setYValueFormatter();
        lcOverviewKpiChart.clear();
        lcOverviewKpiChart.fitScreen();
        lcOverviewKpiChart.setData(ChartHelper.getChartData(chartSpnPosition, bean));
        lcOverviewKpiChart.invalidate();
        lcOverviewKpiChart.animateY(1500, Easing.EasingOption.EaseInOutQuart);
    }


    //  TOP FIVE
    private void loadTopFive() {
        pvTopFive.onStartLoading();
        vnEmptyTopFive.hide();
        if (topFiveSubscription != null) {
            topFiveSubscription.unsubscribe();
        }
        addSubscription(topFiveSubscription = RestService.getInstance().getHomeTopFive(getParams())
                .compose(RxUtils.applySchedulers())
                .subscribe(bean -> {
                    fillTopFive(bean);
                    pvTopFive.onCompleteLoading();
                }, throwable -> {
                    pvTopFive.onErrorLoading(throwable.getMessage());
                }));
    }

    private void fillTopFive(HomeTopFiveBean bean) {
        switch (topFiveSpnPosition) {
            case TOP_FIVE_ACCOUNTS:
                topFiveAdapter.setData(bean.getAccountEntityList());
                ViewUtils.checkEmptyList(bean.getAccountEntityList(), vnEmptyTopFive);
                break;

            case TOP_FIVE_USERS:
                topFiveAdapter.setData(bean.getUserEntityList());
                ViewUtils.checkEmptyList(bean.getUserEntityList(), vnEmptyTopFive);
                break;

            case TOP_FIVE_SENSORS:
                topFiveAdapter.setData(bean.getSensorEntityList());
                ViewUtils.checkEmptyList(bean.getSensorEntityList(), vnEmptyTopFive);
                break;
        }

    }

    private Map<String, String> getCompareParams() {
        HashMap<String, String> params = new HashMap<>();
        Calendar fromDate = DateUtils.createStartDayDate(this.toDate);
        Calendar toDate = DateUtils.createEndDayDate(this.toDate);

        Calendar prevFromDate = DateUtils.createStartDayDate(this.toDate);
        Calendar prevToDate = DateUtils.createEndDayDate(this.toDate);

        switch (filterType) {
            case DAY:
                prevFromDate.add(Calendar.DAY_OF_YEAR, -1);
                prevToDate.add(Calendar.DAY_OF_YEAR, -1);
                break;
            case MONTH:
                fromDate.add(Calendar.MONTH, -1);

                prevFromDate.add(Calendar.MONTH, -2);
                prevToDate.add(Calendar.MONTH, -1);
                break;

            case YEAR:
                fromDate.add(Calendar.YEAR, -1);

                prevFromDate.add(Calendar.YEAR, -2);
                prevToDate.add(Calendar.YEAR, -1);
                break;
        }

        params.put(ResUtils.getString(R.string.params_to), DateUtils.patternFormatting(toDate.getTime(), DateUtils.Date_for_request));
        params.put(ResUtils.getString(R.string.params_from), DateUtils.patternFormatting(fromDate.getTime(), DateUtils.Date_for_request));
        params.put(ResUtils.getString(R.string.params_prev_to), DateUtils.patternFormatting(prevToDate.getTime(), DateUtils.Date_for_request));
        params.put(ResUtils.getString(R.string.params_prev_from), DateUtils.patternFormatting(prevFromDate.getTime(), DateUtils.Date_for_request));

        return params;
    }

    private Map<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();
        Calendar fromDate = DateUtils.createStartDayDate(this.toDate);
        Calendar toDate = DateUtils.createEndDayDate(this.toDate);

        switch (filterType) {
            case MONTH:
                fromDate.add(Calendar.MONTH, -1);
                break;

            case YEAR:
                fromDate.add(Calendar.YEAR, -1);
                break;
        }

        params.put(ResUtils.getString(R.string.params_to), DateUtils.patternFormatting(toDate.getTime(), DateUtils.Date_for_request));
        params.put(ResUtils.getString(R.string.params_from), DateUtils.patternFormatting(fromDate.getTime(), DateUtils.Date_for_request));

        return params;
    }

    private String getComparePercent(float firstValue, float secondValue) {
        if (firstValue == secondValue) {
            return String.format(Locale.UK, "+%d", 0);
        }
        if (firstValue == 0) {
            return String.format(Locale.UK, "+%d", 100);
        }

        int value = (int) ((secondValue / firstValue) * 100 - 100);
        if (value >= 0) {
            return String.format(Locale.UK, "+%d", value);
        } else {
            return String.valueOf(value);
        }
    }

    private IAxisValueFormatter getValueFormatter(int period) {
        switch (period) {
            case MONTH:
                return new MonthValueFormatter();

            case YEAR:
                return new YearValueFormatter();
            default:
                return new DayValueFormatter();
        }
    }

    private void setPercentFormatter() {
        YAxis yAxis = lcOverviewKpiChart.getAxisLeft();
        yAxis.setValueFormatter(new PercentFormatter());
    }

    private void setDecimalFormatter() {
        YAxis yAxis = lcOverviewKpiChart.getAxisLeft();
        yAxis.setValueFormatter(new DefaultAxisValueFormatter(0));
    }

    private void setYValueFormatter() {
        XAxis xAxis = lcOverviewKpiChart.getXAxis();
        xAxis.setValueFormatter(getValueFormatter(filterType));
    }
}
