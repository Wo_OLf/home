package net.overc.mobile.home.chart;


import android.graphics.drawable.Drawable;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import net.overc.mobile.database.models.home.GraphPoint;
import net.overc.mobile.database.models.home.HomeOverviewGraphBean;
import net.overc.mobile.database.models.home.HomeOverviewKPIGraphBean;
import net.overc.mobile.utils.CollectionsUtil;
import net.overc.mobile.utils.ResUtils;

import java.util.ArrayList;


public class ChartHelper {

    public static LineData getChartData(int type, HomeOverviewGraphBean bean) {
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        switch (type) {
            case ChartEntry.GRAPH_OVERVIEW_ALL:
                dataSets.add(generateSingleLineData(bean.getSensorEventsGraphBean(), ChartEntry.GRAPH_OVERVIEW_SENSOR_EVENTS));
                dataSets.add(generateSingleLineData(bean.getMultimediaEventsGraphBean(), ChartEntry.GRAPH_OVERVIEW_MULTIMEDIA));
                dataSets.add(generateSingleLineData(bean.getReportsGraphBean(), ChartEntry.GRAPH_OVERVIEW_REPORTS));
                dataSets.add(generateSingleLineData(bean.getAlertsGraphBean(), ChartEntry.GRAPH_OVERVIEW_ALERTS));
                dataSets.add(generateSingleLineData(bean.getGeofenceAlertsGraphBean(), ChartEntry.GRAPH_OVERVIEW_GEOFENCE));
                break;

            case ChartEntry.GRAPH_OVERVIEW_SENSOR_EVENTS:
                dataSets.add(generateSingleLineData(bean.getSensorEventsGraphBean(), ChartEntry.GRAPH_OVERVIEW_SENSOR_EVENTS));
                break;
            case ChartEntry.GRAPH_OVERVIEW_MULTIMEDIA:
                dataSets.add(generateSingleLineData(bean.getMultimediaEventsGraphBean(), ChartEntry.GRAPH_OVERVIEW_MULTIMEDIA));
                break;
            case ChartEntry.GRAPH_OVERVIEW_REPORTS:
                dataSets.add(generateSingleLineData(bean.getReportsGraphBean(), ChartEntry.GRAPH_OVERVIEW_REPORTS));
                break;
            case ChartEntry.GRAPH_OVERVIEW_ALERTS:
                dataSets.add(generateSingleLineData(bean.getAlertsGraphBean(), ChartEntry.GRAPH_OVERVIEW_ALERTS));
                break;
            case ChartEntry.GRAPH_OVERVIEW_GEOFENCE:
                dataSets.add(generateSingleLineData(bean.getGeofenceAlertsGraphBean(), ChartEntry.GRAPH_OVERVIEW_GEOFENCE));
                break;
        }

        return new LineData(dataSets);
    }

    public static LineData getSingleChartData(HomeOverviewKPIGraphBean bean, int type) {
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(generateSingleLineData(bean, type));
        return new LineData(dataSets);
    }


    private static LineDataSet generateSingleLineData(HomeOverviewKPIGraphBean bean, int type) {
        ArrayList<Entry> value = new ArrayList<>();
        if (CollectionsUtil.isNotNullAndNotEmpty(bean.getPoints())) {
            for (GraphPoint point : bean.getPoints()) {
                value.add(new ChartEntry(point.getKey().getTime(), point.getValue(),type));
            }
        }

        LineDataSet dataSet = new LineDataSet(value, bean.graphName());
        dataSet.setColor(bean.lineColor());
        dataSet.setLineWidth(2);

        dataSet.setDrawCircles(false);
//        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setDrawValues(false);
        dataSet.enableDashedHighlightLine(10f, 5f, 0f);
        dataSet.setHighLightColor(bean.lineColor());
        if (bean.useGradient()) {
            dataSet.setDrawFilled(true);
            Drawable background = ResUtils.getDrawable(bean.gradientResId());
            dataSet.setFillDrawable(background);
        } else {
            dataSet.setDrawFilled(false);
        }


        return dataSet;
    }


}
