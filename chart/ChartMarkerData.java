package net.overc.mobile.home.chart;

public class ChartMarkerData {

    private float value;
    private ChartRanges ranges;


    public ChartMarkerData(float value, ChartRanges ranges) {
        this.value = value;
        this.ranges = ranges;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public ChartRanges getRanges() {
        return ranges;
    }

    public void setRanges(ChartRanges ranges) {
        this.ranges = ranges;
    }
}
