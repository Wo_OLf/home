package net.overc.mobile.home.chart;

/**
 * Created by ghost on 06.04.2016.
 */
public class ChartRanges {

    private float Xmin;
    private float Xmax;
    private float Ymin;
    private float Ymax;

    public ChartRanges(float xmin, float xmax, float ymin, float ymax) {
        Xmin = xmin;
        Xmax = xmax;
        Ymin = ymin;
        Ymax = ymax;
    }

    public float getXmin() {
        return Xmin;
    }

    public void setXmin(float xmin) {
        Xmin = xmin;
    }

    public float getXmax() {
        return Xmax;
    }

    public void setXmax(float xmax) {
        Xmax = xmax;
    }

    public float getYmin() {
        return Ymin;
    }

    public void setYmin(float ymin) {
        Ymin = ymin;
    }

    public float getYmax() {
        return Ymax;
    }

    public void setYmax(float ymax) {
        Ymax = ymax;
    }
}
