package net.overc.mobile.home.chart;


import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import net.overc.mobile.utils.DateUtils;

import java.util.Calendar;

public class YearValueFormatter implements IAxisValueFormatter {
    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis((long) value);
        return DateUtils.patternFormatting(calendar.getTime(), DateUtils.MMMM);
    }
}
