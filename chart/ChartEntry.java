package net.overc.mobile.home.chart;


import android.os.Parcel;

import com.github.mikephil.charting.data.Entry;

public class ChartEntry extends Entry {

    public static final int GRAPH_OVERVIEW_ALL = 0;
    public static final int GRAPH_OVERVIEW_SENSOR_EVENTS = 1;
    public static final int GRAPH_OVERVIEW_MULTIMEDIA = 2;
    public static final int GRAPH_OVERVIEW_REPORTS = 3;
    public static final int GRAPH_OVERVIEW_ALERTS = 4;
    public static final int GRAPH_OVERVIEW_GEOFENCE = 5;
    public static final int GRAPH_KPI = 6;

    private int chartType;

    public ChartEntry() {
    }

    public ChartEntry(float x, float y,int chartType) {
        super(x, y);
        this.chartType = chartType;
    }


    public ChartEntry(Parcel in) {
        super(in);
    }

    public int getChartType() {
        return chartType;
    }

    public void setChartType(int chartType) {
        this.chartType = chartType;
    }
}
