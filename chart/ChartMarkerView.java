package net.overc.mobile.home.chart;


import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

import net.overc.mobile.R;
import net.overc.mobile.utils.DateUtils;
import net.overc.mobile.utils.ViewUtils;

import java.text.MessageFormat;
import java.util.Date;

public class ChartMarkerView extends MarkerView {

    boolean isTOP = true;
    boolean isLEFT = true;

    private float padding = 0;
    private String valueFormatter = "{0}";

    private ChartRanges ranges;
    private ChartMarkerData markerData;

    private TextView tvValue;
    private TextView tvType;
    private TextView tvDate;


    private RelativeLayout rlLeftTop;
    private RelativeLayout rlRightTop;
    private RelativeLayout rlLeftBottom;
    private RelativeLayout rlRightBottom;
    private View viewLeft;
    private View viewRight;

    public ChartMarkerView(Context context) {
        super(context, R.layout.view_chart_dot);
        tvValue = (TextView) findViewById(R.id.tv_chart_value);
        tvType = (TextView) findViewById(R.id.tv_chart_type);
        tvDate = (TextView) findViewById(R.id.tv_chart_date);
        rlLeftTop = (RelativeLayout) findViewById(R.id.rl_chart_top_left);
        rlRightTop = (RelativeLayout) findViewById(R.id.rl_chart_top_right);
        rlLeftBottom = (RelativeLayout) findViewById(R.id.rl_chart_bottom_left);
        rlRightBottom = (RelativeLayout) findViewById(R.id.rl_chart_bottom_right);
        viewLeft = findViewById(R.id.view_chart_left);
        viewRight = findViewById(R.id.view_chart_right);
        padding = ViewUtils.convertDpToPixel(8, context);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        super.refreshContent(e, highlight);
        if (e instanceof ChartEntry) {
            ChartEntry chartEntry = (ChartEntry) e;
            if (chartEntry.getData() != null) {
                switch (chartEntry.getChartType()) {
                    case ChartEntry.GRAPH_OVERVIEW_SENSOR_EVENTS:
                        tvType.setText(R.string.label_home_sensor_events);
                        valueFormatter = "{0}";
                        break;
                    case ChartEntry.GRAPH_OVERVIEW_ALERTS:
                        tvType.setText(R.string.label_home_alerts);
                        valueFormatter = "{0}";
                        break;
                    case ChartEntry.GRAPH_OVERVIEW_GEOFENCE:
                        tvType.setText(R.string.label_home_geofence);
                        valueFormatter = "{0}";
                        break;
                    case ChartEntry.GRAPH_OVERVIEW_MULTIMEDIA:
                        tvType.setText(R.string.label_home_multimedia);
                        valueFormatter = "{0}";
                        break;
                    case ChartEntry.GRAPH_OVERVIEW_REPORTS:
                        tvType.setText(R.string.label_home_reports);
                        valueFormatter = "{0}";
                        break;
                    case ChartEntry.GRAPH_KPI:
                        tvType.setText(R.string.label_home_sensor_monitor);
                        valueFormatter = "{0} %";
                        break;
                }
                tvType.requestLayout();
                markerData = (ChartMarkerData) e.getData();
                ranges = markerData.getRanges();

                defineFourth(ranges, e.getX(), e.getY());
                setPointVisibility();

                long date = (long) e.getX();
                tvDate.setText("at " + DateUtils.patternFormatting(new Date(date), DateUtils.HH_mm));
                int value = (int) markerData.getValue();
                tvValue.setText(MessageFormat.format(valueFormatter, value));
            } else if (e.getData() != null) {
                markerData = (ChartMarkerData) e.getData();
                ranges = markerData.getRanges();
                tvType.setText(R.string.no_data);
                tvValue.setText(R.string.no_data);
                tvDate.setText(R.string.no_data);
                defineFourth(ranges, e.getX(), e.getY());
                setPointVisibility();
            }
        }
    }


    @Override
    public MPPointF getOffset() {
        MPPointF pointF = new MPPointF();
        pointF.x = isLEFT ? 0 - padding : padding - getWidth();
        pointF.y = isTOP ? 0 - padding : padding - getHeight();
        return pointF;
    }

    private void defineFourth(ChartRanges ranges, float x, float y) {
        isLEFT = x <= (ranges.getXmax() + ranges.getXmin()) / 2;
        isTOP = y > (ranges.getYmax() + ranges.getYmin()) / 2;
    }

    private void setPointVisibility() {
        rlLeftBottom.setVisibility(isLEFT && !isTOP ? VISIBLE : INVISIBLE);
        rlRightBottom.setVisibility(!isLEFT && !isTOP ? VISIBLE : INVISIBLE);
        rlLeftTop.setVisibility(isLEFT && isTOP ? VISIBLE : INVISIBLE);
        rlRightTop.setVisibility(!isLEFT && isTOP ? VISIBLE : INVISIBLE);
        viewLeft.setVisibility(isLEFT ? VISIBLE : INVISIBLE);
        viewRight.setVisibility(!isLEFT ? VISIBLE : INVISIBLE);
    }
}
