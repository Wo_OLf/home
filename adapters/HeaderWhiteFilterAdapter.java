package net.overc.mobile.home.adapters;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.overc.mobile.AdminApp;
import net.overc.mobile.R;

import java.util.List;

public class HeaderWhiteFilterAdapter extends ArrayAdapter<String> {

    private List<String> values;

    public HeaderWhiteFilterAdapter(List<String> values) {
        super(AdminApp.getInstance(), R.layout.item_header, values);
        this.values = values;
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return values.get(position);
    }


    @NonNull
    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) AdminApp.getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_header, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTitle.setText(values.get(position));

        return convertView;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) AdminApp.getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_header_blue, parent, false);
            holder = new HeaderViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        holder.tvTitle.setText(values.get(position));

        return convertView;
    }

    class ViewHolder {
        TextView tvTitle;

        public ViewHolder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tv_item_header);
        }
    }

    class HeaderViewHolder {
        TextView tvTitle;

        public HeaderViewHolder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tv_item_header_blue);
        }
    }
}
