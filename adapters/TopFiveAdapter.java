package net.overc.mobile.home.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.overc.mobile.Const;
import net.overc.mobile.R;
import net.overc.mobile.base.FillUserInfo;
import net.overc.mobile.core.tree.common.BaseRecyclerAdapter;
import net.overc.mobile.database.models.AccountModel;
import net.overc.mobile.database.models.SensorModel;
import net.overc.mobile.database.models.UserModel;
import net.overc.mobile.markerinterface.IBaseEntity;


public class TopFiveAdapter extends BaseRecyclerAdapter<IBaseEntity, TopFiveAdapter.ValueHolder> {

    public TopFiveAdapter(Context context) {
        super(context);
    }

    @Override
    public ValueHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ValueHolder(inflateHolder(R.layout.item_top_five, parent));
    }

    @Override
    public void onBindViewHolder(ValueHolder holder, int position) {
        IBaseEntity model = getItemByPosition(position);

        fillItem(holder, model);
        setupListener(holder.rootView, position, model);
    }

    private void fillItem(ValueHolder holder, IBaseEntity model) {
        switch (model.getEntityId()) {
            case Const.USER_ENTITY_ID:
                UserModel userModel = (UserModel) model;
                FillUserInfo.loadAvatar(userModel, holder.ivIcon);
                FillUserInfo.fillUserName(userModel, holder.tvValue);
                break;

            case Const.SENSOR_ENTITY_ID:
                SensorModel sensorModel = (SensorModel) model;
                holder.tvValue.setText(sensorModel.getName());
                holder.ivIcon.setImageResource(R.drawable.ic_fab_admin_sensors);
                break;

            default:
                AccountModel accountModel = (AccountModel) model;
                holder.tvValue.setText(accountModel.getAccountName());
                holder.ivIcon.setImageResource(R.drawable.ic_fab_admin_account);
                break;
        }

    }

    static class ValueHolder extends RecyclerView.ViewHolder {

        View rootView;
        TextView tvValue;
        ImageView ivIcon;


        ValueHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            tvValue = (TextView) itemView.findViewById(R.id.tv_item_top_five_value);
            ivIcon = (ImageView) itemView.findViewById(R.id.iv_item_top_five);


        }
    }

}
